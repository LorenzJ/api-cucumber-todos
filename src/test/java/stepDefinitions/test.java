package stepDefinitions;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import static org.junit.Assert.assertEquals;

public class test {
    String apiUrl = "https://jsonplaceholder.typicode.com/todos/";
    HttpResponse<JsonNode> firstTodoResponse;

    @Given("^Something's up")
    public void something_is_up() {
    }
    
    @When("^I request the second todo$")
    public void i_request_the_second_todo() throws  UnirestException {
    }

    @Then("^the second todo is returned$")
    public void the_first_todo_is_returned() {
    }
}
