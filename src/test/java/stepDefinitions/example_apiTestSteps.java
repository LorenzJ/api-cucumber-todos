package stepDefinitions;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import static org.junit.Assert.assertEquals;

public class example_apiTestSteps {
    String apiUrl = "https://jsonplaceholder.typicode.com/todos/";
    HttpResponse<JsonNode> firstTodoResponse;

    @Given("^The todo api is up$")
    public void the_todo_api_is_up() throws UnirestException {
        HttpResponse<JsonNode> todosResponse = Unirest.get(apiUrl).asJson();

        assertEquals(200, todosResponse.getStatus());
    }

    @When("^I request the first todo$")
    public void i_request_the_first_todo() throws  UnirestException {
        firstTodoResponse = Unirest.get(apiUrl + "1").asJson();

        assertEquals(200, firstTodoResponse.getStatus());
    }

    @Then("^the first todo is returned$")
    public void the_first_todo_is_returned() {
        String expectedResponse = "{\"id\":1,\"completed\":false,\"title\":\"delectus aut autem\",\"userId\":1}";
        String receivedResponse = firstTodoResponse.getBody().getArray().get(0).toString();

        assertEquals(expectedResponse,receivedResponse);
    }

}
